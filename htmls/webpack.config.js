/**
 * Created by satishgowda on 16/07/16.
 */
var path = require('path');
var webpack = require('webpack');
var commonsPlugins = new webpack.optimize.CommonsChunkPlugin('shared.js');
module.exports = {
    context:path.resolve('js'),
    entry:{
        home:'./home_page',
        about:'./about_page',
        contact:'./contact_page'
    },
    output:{
        path:path.resolve('build/js/') ,
        publicPath:'/public/assets/js',
        filename:'[name].js'
    },
    plugins:[
        commonsPlugins,
        new webpack.ProvidePlugin({
            $:'jquery',
            jQuery:'jquery',
            'window.jQuery':'jquery'
        })
    ],
    devServer:{
        contentBase:'public'
    },
    module:{
        preLoaders:[
            {
                test:/\.js/,
                exclude:/node_modules/,
                loader:'jshint-loader'
            }
        ],
        loaders:[
            {
                test :/\.es6/,
                exclude:/node_modules/,
                loader:'babel-loader'
            }
        ]
    },
    resolve:{
      extensions:['','.js','.es6']
    },
    watch : true
};
